! ==================== !
! Programming Practice !
! ==================== !
! Venkata Krisshna

! Can you sum?
! Daily problem - 7 Aug 2019

program queen_attack
  implicit none
  integer,dimension(:),allocatable :: list
  integer :: n,num

  print*,'==================================='
  print*,'Programming Practice - Can you sum?'
  print*,'==================================='
  call init
  call solve

contains

  ! Read list and magic number
  subroutine init
    implicit none
    integer,dimension(:),allocatable :: input

    n = 10 ! Number of entries on the list
    allocate(input(1:n+1))
    open(21, file="input")
    read(21,*) input
    allocate(list(1:n))
    list = input(1:n)
    num = input(n+1)

    return
  end subroutine init

  ! Determine if sum is possible
  subroutine solve
    implicit none
    integer :: i,j
    logical :: sum=.false.
    
    do i = 1,n
       do j = 1,n
          if(i.ne.j .and. list(i)+list(j).eq.num) then
             print*,'Here! ->',list(i),'+',list(j)
             sum = .true.
          end if
       end do
    end do
    if (.not.sum) print*,'No possibility to obtain the magic number as a sum!'
    
    return
  end subroutine solve
  
end program queen_attack
